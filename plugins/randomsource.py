# random.py
# randomising source plugin for awpgen

import random

from core.source import *

class RandomSource(AwpgenSource):
	choice=None
	@classmethod
	def name(cls):
		if RandomSource.choice is not None: return RandomSource.choice
		return "random"
	@classmethod
	def description(cls):
		return "Randomly choose a source plugin"
	
	def get_image(self,params):
		"""
		Randomly choose a plugin
		"""
		
		plugins = find_source_plugins()
		if 'random' in plugins: del plugins['random'] # exclude self
		
		choice = random.choice([k for k in plugins.keys()])
		print(f"Chose {choice} plugin")
		cls = plugins[choice]
		
		instance = cls(self._config)
		RandomSource.choice = cls.name()
		return instance.get_image(params)
 
