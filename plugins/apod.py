# apod.py
# APOD source plugin for awpgen
import random, os

from datetime import date
from collections import OrderedDict

from lxml import html

from pyvips import Image

from core.source import AwpgenSource
from core.helpers import *

from radrequests import requests 

class ApodSource(AwpgenSource):
	@classmethod
	def name(cls):
		return "apod"
	@classmethod
	def description(cls):
		return "Download an image from NASA's Astronomy Picture Of The Day website"
	
	@classmethod
	def setup_args(cls,parser):
		parser.add_argument(
			'-a','--apod-url',
			action='store',dest="apod_url",metavar="URL",
			type=str, #argparse.FileType('r'),
			help="Specify an apod image by its URL (e.g apXXXXXX.html, default: choose randomly)"
		)
		
		parser.set_defaults(
			apod_url = None
		)
		
	
	
	apod_base = "https://apod.nasa.gov/apod/"
	
	apod_index = "archivepixFull.html"
	
	def get_index(self):
		"""
		Retrieve, parse, and return the APOD index
		"""
		
		#TODO: Be nice to NASA: Only download the full index once, then
		# load it from disk and mux it together with the latest /archivepix.html
		
		print(" - Retrieving APOD index...")
		resp = requests.get(self.apod_base + self.apod_index,cache_mins=1440) #1440 mins in a day
		xml = html.fromstring(resp.text)
		links = xml.xpath("//body/b/a")
		print(f" - got {len(links)} items.")
		
		# url should look like ap220514.html
		# we should be able to get the date from this if we need it, 22/may/2014
		ret = OrderedDict()
		for a in links:
			url = a.get('href')
			title=a.text_content()
			when=None
			if url.startswith('ap') and url.endswith('.html') and len(url) == 13:
				year=int(url[2:4])
				# This will have a year 2100 problem because we only have a 2-digit year
				if year>=95: year += 1900
				else: year += 2000
				
				when = date(year=year,month=int(url[4:6]), day=int(url[6:8]))
				#print(when)
			ret[url] = {'title':title, 'url': a.get('href'), 'date': when }
			
		# TODO: Exclusions
		
		return ret
	
	def add_exclusion(self,item,filename="exclusions.auto"):
		"""
		add an exclusion for the specified URL
		"""
		pass
	
	def get_image(self,opts):
		"""
		Generate and return an APOD image
		"""
		print("Generating APOD image")
		
		index = self.get_index()
		
		urls = [ str(i) for i in index.keys() ]
		
		if opts.apod_url is None:
			choice = index[random.choice(urls)]
		else:
			choice = index[opts.apod_url]
		
		print(f" - Chose '{choice['title']}' ({choice['date'].strftime('%d-%b-%Y')})")
		
		#TODO:
		"""
		The APOD Website is pretty static once each image is published.
		We can make our code a) faster and b) nicer to nasa if we additively
		 build up a map of the apod site over time.
		 
		* Download the full index once, then download the regular archivepix
			page once a day and update our index. We'll probably need a way
			to detect holes (i.e if they change their archive date)
			and re-download the full index if necessary
			
		* Only download and parse each image's webpage (apXXXXXX.html) once,
			saving the info in our data file.
			
		
		 
		"""
		url = self.apod_base + choice['url']
		print(f" - Retrieving and parsing '{url}'...")
		resp = requests.get(url,cache_mins=10080) # cache for a week. TODO: cache cleaner, this will grow large over time
		
		xml = html.fromstring(resp.text)
		eles = xml.xpath("//body//p/a[count(img)=1]")
		if len(eles) == 0:
			print("Error: No usable image found!")
			self.add_exclusion(choice['url'])
			# try another
		#elif len(eles) != 1:
		#	# Too many images!
		image_url = self.apod_base + eles[0].get('href')
		
		print(f" - Downloading '{image_url}'...")
		resp = requests.get(image_url,progressbar=True,cache_mins=10080) 
		
		img = Image.new_from_buffer(resp._content,"")
		
		#save original image
		save_jpg(img,os.path.expanduser("~/.awpgen/apod_image.jpg"))
		
		dw,dh = desktop_size()
		img = fitscale(img,dw,dh)
		
		"""
		if w > dw and h > dh:
			
			if w > dw:
				sc = dw/w
				nh = h*sc
				if nh < dh:
			
			# scale down
			if w >= h: #landscape/square
				sc = dw / w
			else: # portrait
				sc = dh / h
			
			sct = "%1.2f" % sc
			scw = int(round(w * sc))
			sch = int(round(h * sc))
			print(f" - Image is {w}x{h}, scaling by {sct} to {scw}x{sch}")
			img = img.resize(sc)
		"""
		
		img = img.smartcrop(dw,dh)
		
		# Add title
		
		title = draw_text(choice['title'],width=dw,height=16).copy(interpretation="srgb")
		
		title = title.gravity('south-east',dw,dh).copy(interpretation="srgb")
		
		#title.tiffsave('/tmp/foo.tiff')
		
		#offset:
		ox = oy = 0
		tmp = str(opts.title_offset).split(',')
		if len(tmp) == 2:
			ox = int(tmp[0])
			oy = int(tmp[1])
		else:
			print(f"Warning: Invalid minimap offset '{opts.title_offset}")
		
		img = img.composite(title,'atop',x=ox*-1,y=oy*-1)
		
		#img.tiffsave('/tmp/foo2.tiff')
		
		return img
 
