"""
APOD Image Source Plugins

Add a new python file here containing a new subclass of AwpgenSource, 
 and your new image source will be supported by awpgen

Awpgen automatically loads all python modules in the plugins directory,
 see https://stackoverflow.com/questions/1057431/how-to-load-all-modules-in-a-folder#1057534

"""
from os.path import dirname, basename, isfile, join
import glob
modules = glob.glob(join(dirname(__file__), "*.py"))
__all__ = [ basename(f)[:-3] for f in modules if isfile(f) and not f.endswith('__init__.py')]
