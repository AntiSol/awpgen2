# antisol.py
# awpgen source plugin to download astro images from antisol.org/gallery

import random, html, os

from pyvips import Image

from core.helpers import desktop_size, fitscale, draw_text

from core.source import AwpgenSource

from radrequests import requests

class AntiSolSource(AwpgenSource):
	
	base_url = 'https://antisol.org/gallery/'
	
	@classmethod
	def name(cls):
		return "antisol"
	@classmethod
	def description(cls):
		return "Download an image from AntiSol's Photo Gallery"
	
	def get_image(self,opts):
		"""
		Generate and return an image
		"""
		
		print(" - Downloading AntiSol's image index...")
		resp = requests.get(self.base_url + 'astro.json',cache_mins = 1440) #1440 mins in a day
		if resp.status_code != 200:
			print("Error")
			return None
		
		data = resp.json()
		print(f" - Got {len(data)} images.")
		item = random.choice(data)
		
		desc = html.unescape(item['description'])
		if not desc:
			desc=item['image'] + " from antisol.org"
			
		print(f" - Chose '{desc}'")
		url = self.base_url + item['image']
		print(f" - Downloading '{url}'...")
		resp = requests.get(url,save_as=os.path.expanduser("~/.awpgen/antisol_image.jpg"),progressbar=True)
		if resp.status_code != 200:
			#error
			print("Error!")
			return None
			
		
		img = Image.new_from_buffer(resp._content,"")
		dw,dh = desktop_size()
		img = fitscale(img,dw,dh).smartcrop(dw,dh)
		
		title = draw_text(desc,width=dw,height=16).copy(interpretation="srgb")
	
		title = title.gravity('south-east',dw,dh).copy(interpretation="srgb")
		
		#title.tiffsave('/tmp/foo.tiff')
		
		#offset:
		ox = oy = 0
		tmp = str(opts.title_offset).split(',')
		if len(tmp) == 2:
			ox = int(tmp[0])
			oy = int(tmp[1])
		else:
			print(f"Warning: Invalid minimap offset '{opts.title_offset}")
		
		img = img.composite(title,'atop',x=ox*-1,y=oy*-1)
		
		return img

