# deepfield.py
# Deep-field source plugin for awpgen

import argparse, os, glob, random

from textwrap import wrap

from core.source import AwpgenSource
from core.helpers import *

from pyvips import Image
from screeninfo import get_monitors


class DeepFieldSource(AwpgenSource):
	@classmethod
	def name(cls):
		return "deepfield"
	@classmethod
	def description(cls):
		return "Extract a random area from a large image"
	
	@classmethod
	def setup_args(cls,parser):
		parser.add_argument(
			'-d','--deepfield-image',
			action='store',dest="deepfield_image",metavar="IMAGE",
			type=str, #argparse.FileType('r'),
			help="Specify a deepfield image (default: choose randomly)"
		)
		parser.add_argument(
			'--minimap-size',type=int,dest="minimap_size",
			help="Set Minimap size"
		)
		
		parser.add_argument('--allow-unoptimised', help="Allow unoptimised images (implied by -d)",
			dest="allow_unoptimised", action='store_true'
		)
		
		parser.add_argument('--no-warn-unoptimised', help="Suppress warning about unoptimised images",
			dest="warn_unoptimised", action='store_false'
		)
		
		#custom action to optimise an image
		parser.add_argument(
			'--optimise-image',action='store_const',dest="run_and_exit",const=DeepFieldSource.optimise_image_action,
			help="Optimise an image (use -d to specify image)"
		)
		
		parser.add_argument(
			'--get',action='store_const',dest="run_and_exit",const=DeepFieldSource.get_image_action,
			help="Download a known Deep Field image (use -d to specify image, --library to see a list)"
		)
		
		parser.add_argument(
			'--library',action='store_const',dest="run_and_exit",const=DeepFieldSource.library_action,
			help="Show a list of known Deep Field Images available for download with --get"
		)
		
		parser.add_argument(
			'--full-library',action='store_const',dest="run_and_exit",const=DeepFieldSource.full_library_action,
			help="Show a list of known Deep Field Images available for download with --get, with descriptions."
		)
		
		parser.add_argument('--fail-horribly', help="Try to use vips callbacks",
			dest="fail_horribly", action='store_true'
		)
		
		parser.set_defaults(
			fail_horribly=False,
			minimap_size=256,
			allow_unoptimised=None,
			warn_unoptimised=True
		)
	
	# load the library
	@staticmethod
	def load_library() -> dict:
		ret = []
		library_file = os.path.join(os.path.dirname(__file__),"deepfields.json")
		if os.path.exists(library_file):
			print(f"Loading library from {library_file}")
			with open(library_file,'r') as f:
				ret = json.load(f)
			print(f" - got {len(ret)} items")
		return sort_by(ret,'date',int)
		
	@staticmethod
	def save_library():
		#TODO
		pass

	@staticmethod
	def get_image_action(opts) -> int:
		"""
		Downloads a known deepfield image from the library
		 for this action, we expect -d to be either an int index from the 
		 library, or a search term (id / title)
		"""
		
		choice=None
		library = DeepFieldSource.load_library()
		search=opts.deepfield_image
		if not search: 
			print("Error: you need to specify a deepfield to download with -d <number or search>. Use --library for a list")
			return 1
			
		# special case: 'all':
		if str(search).lower().strip() == "all":
			totalsize = friendly_size(sum([x['bytes'] for x in library]))
			if not yesno(f"Warning: You are about to download an actual fucktonne of data ({totalsize}!). Proceed (Y/N)?"):
				return 1
			choice=library
			
		if str(search).isnumeric():
			# it's an index:
			idx = int(search)
			if idx in library:
				choice=[library[idx]]
				
		if not choice and search:
			search = str(search).lower().strip()
			for itm in library:
				if search in itm['id'].lower() or search in itm['title'].lower():
					choice=[itm]
					break
					
		if not choice:
			print("No matching library image found, specify one with -d")
			return 1
		
		#print("Downloading ",choice)
		
		if not isinstance(choice,list):
			choice = [choice]
		
		for c in choice:
		
			#TODO: splitext and .tiff
			destfile = os.path.expanduser("~/.awpgen/deepfield_" + c['filename'])
			
			if os.path.exists(destfile):
				print(f"Can't download: '{destfile}' already exists!")
				continue
			
			tmpfile="/tmp/" + c['filename']
			download_file(c['url'],tmpfile,i_insist_it_exists=True)
			DeepFieldSource.optimise_image(tmpfile,destfile)
			#TODO: 
			DeepFieldSource.get_minimap(destfile)
			
			#TODO: delete original? Is this an option, or a prompt? maybe both?
			
			#return 0
	
	@staticmethod
	def full_library_action(opts,full=False) -> int:
		DeepFieldSource.library_action(opts,full=True)
	
	@staticmethod
	def library_action(opts,full=False) -> int:
		#print(opts)
		library = DeepFieldSource.load_library()
		#print(library)
		
		textwidth = 78
		print()
		print("-"*textwidth)
		print("THE AWPGEN LIBRARY\n\nUse e.g '--get -d 0' or '--get -d opo9601' to download and install an image.\n\nAWPGen Knows about the following deepfield images:")
		#print("-"*textwidth)
		
		n=0
		for itm in library:
			w=itm['width']
			h=itm['height']
			bytes=itm['bytes']
			disk=itm['bytes_optimised']
			id=itm['id'].strip()
			if id:
				id=f" ({id})"
				
			
			description="\n".join(wrap(itm['description'],textwidth,
				subsequent_indent=" ",fix_sentence_endings=True,
				tabsize=4
			))
			print("-"*textwidth)
			print(f"{n}: {itm['title']}{id}")
			print("-"*textwidth)
			print(f"Size: {w}x{h} ({friendly_pixelcount(w,h)})")
			print(f"      {friendly_size(bytes)} download, {friendly_size(disk)} on disk.")
			print(f"Released: {itm['date']}")
			print(f"Source: {itm['source']}")
			#print("-"*20)
			
			if full:
				print(f"Description:\n\n{description}")
				
			print()
			n+=1
		
		return 0	
	
	@staticmethod
	def optimise_image_action(opts) -> int:
		"""
		Saves an optimised copy of the specified image (and a minimap) in the .awpgen directory
		If the provided image is a deepfield, renames it so that it will no longer match 'deepfield_'
		"""
		if opts.deepfield_image is None:
			print("\nError: you must specify an image to optimise with -d <filename>\n")
			return 1
			
		if opts.output is None:
			print("\nError: you must specify an output image with -o <filename>\n")
			return 1
			
		DeepFieldSource.optimise_image(opts.deepfield_image,opts.output,opts)
		
		return 0
		
	@staticmethod
	def optimise_image(sourcefile,destfile,opts=None,compression:str='jpg',jpeg_quality=80) -> Image:
		"""
		Generalised helper to optimise an image. Does all the heavy lifting:
		 - converts the specified file into tiled tiff format optimised for
		 	using as a deepfield.
		 - generates a minimap for the image
		 - can write e.g a title into a json info file for the image
		 - saves the image with the specified dest name, along with sidecar files.
		 - optionally (but by default), if the source image and dest image 
			are both returned by find_deepfields, the source is renamed so 
			that it will no longer be found (to prevent auto-loading of unoptimised duplicates)
		"""
		
		print(f"Optimising '{sourcefile}' for deepfield...")
		image = Image.new_from_file(sourcefile)
		
		# save the image
		DeepFieldSource.save_tiff(image,destfile)
		
		#minimap = DeepFieldSource.generate_minimap(image,opts.minimap_size)
		#minimap.hpegsave(minimapfile)
		
		#json = get_json()
		#josn.write_to_file(jsonsidecar)
		
	
	@staticmethod
	def save_tiff(image: Image, filename:str,compression:str='jpeg',jpeg_quality:int=80):
		ret= image.tiffsave(
			filename,
			tile=True,
			bigtiff=True,
			pyramid=False,
			compression=compression,		# we use jpeg compression in our tiled tiff
			Q=jpeg_quality,					# for a relatively small file size
		)
		filesize = friendly_size(os.path.getsize(filename))
		print(f"Wrote '{filename}', {filesize}")
		return ret
	
	def find_deepfields(self,extra_paths=[]):
		"""
		Return a list of all deepfield images found in the search paths
		"""
		paths = [
			'~/.awpgen',
			'/usr/share/awpgen',
			'/usr/local/share/awpgen',
		] + extra_paths
		
		ret = []
		for path in paths:
			path = os.path.expanduser(path)
			files = glob.glob(os.path.join(path,'deepfield_*'))
			ret += files
		
		return ret
	
	def get_image(self,opts):
		"""
		Generate and return a deepfield image
		"""
		print("Generating Deepfield image...")
		sourcefile=opts.deepfield_image
		if sourcefile is None:
			deepfields = self.find_deepfields()
			sourcefile = random.choice(deepfields)
			
			if opts.allow_unoptimised is None:
				# the default behaviour for allow_unoptimised depends on
				# whether a deepfield file was specified (if yes, allow)
				opts.allow_unoptimised=False
		else: 
			if opts.allow_unoptimised is None:
				opts.allow_unoptimised=True
		
		
		if not os.path.exists(sourcefile):
			raise ValueError(f"File '{sourcefile}' does not exist!")
			
		filesize = friendly_size(os.path.getsize(sourcefile))
		print(f" - using deepfield '{sourcefile}' ({filesize})")
		
		return self.rand_region(sourcefile,opts)
	
	def progress_cb(self,image,progress):
		print("progress:",progress)	
		
	def rand_region(self,filename:str, opts):
		"""
		Extract a random region of the specified size from the speficied image file.
		"""
		image = Image.new_from_file(filename,access="sequential")
		scale=1.0
		
		if opts.fail_horribly:
			image.set_progress(True)
			image.signal_connect('preeval', self.progress_cb)
			image.signal_connect('eval', self.progress_cb)
			image.signal_connect('posteval', self.progress_cb)
		
		if not is_tiled(image):
			msg = f"'{filename}' is not saved in a tiled format optimised for random access!\n You should let awpgen convert it to a tiled tiff for much much faster execution and smaller memory footprint."
			if not opts.allow_unoptimised:
				print("ERROR: " + msg)
				return None
			
			if opts.warn_unoptimised:
				print("WARNING: " + msg)
				
			image = Image.new_from_file(filename) # load normally
		
		imgw, imgh = image.get('width'), image.get('height')
		desc = get_description(image)
		
		dw, dh = desktop_size()
		w = dw
		h = dh
		
		factor = "%1.2f" % ((imgw*imgh) / (dw*dh))
		pixels = friendly_pixelcount(imgw,imgh)
		print(f" - Image is {imgw}x{imgh} ({pixels}, {factor}x your desktop)")
		if desc:
			print(f" - Description: {desc}")
		
		w /= scale
		h /= scale
		w=int(w)
		h=int(h)
		
		#if the screen is too big for the image, scale it up:
		if w > imgw:
			nscale=w/imgw
			image = image.resize(nscale)
			imgh = int(round(imgh * (nscale)))
			imgw = w
			nscale = "%1.3f" % nscale
			print(f" - Desktop wider than image, scaled to {imgw}x{imgh} ({nscale}x)")
			
		if h > imgh:
			nscale=h/imgh
			image = image.resize(nscale)
			imgw = int(round(imgw * (nscale)))
			imgh = h
			scale = "%1.3f" % nscale
			print(f" - Desktop taller than image, scaled to {imgw}x{imgh} ({nscale}x)")
		
		x = int(round(random.uniform(0,imgw - w)))
		y = int(round(random.uniform(0,imgh - h)))
		
		print(f" - Cropping {w}x{h}@{x},{y}")
		output = image.crop(x,y,w,h).resize(scale)
		
		
		#output.set_progress(True)
		#output.signal_connect('eval', self.progress_cb)
		#output.signal_connect('posteval', self.progress_cb)
		
		minimap, sc, tw, th = DeepFieldSource.get_minimap(filename,image,opts.minimap_size)
		
		#scale is returned by get_minimap
		#sc = tw / imgw
		#annotation layer x/y/w/h
		ax = int(round(x * sc))
		ay = int(round(y * sc))
		aw = int(round(w * sc))
		ah = int(round(h * sc))
		
		#offset:
		ox = oy = 0
		tmp = str(opts.title_offset).split(',')
		if len(tmp) == 2:
			ox = int(tmp[0])
			oy = int(tmp[1])
		else:
			print(f"Warning: Invalid minimap offset '{opts.title_offset}")
		
		#print(tw,th,ax,ay,aw,ah,sc)
		print(" - Drawing Minimap...")
		minimap = minimap
		
		# RGBA values for border and region areas on minimap:
		border_colour = (128,128,128,64)
		region_colour = (255,255,255,64)
		region_border = (64,64,64,64)
		
		annotations = transparent_image(tw+2,th+2)
		
		annotations = annotations.draw_rect(
			border_colour,
			0,0,tw+2,th+2,
			fill=False
		).draw_rect(
			border_colour,
			1,1,tw+1,th+1,
			fill=False
		) # +2 stuff is because we want a 2-pixel border
		
		#draw the region:
		annotations = annotations.draw_rect(
			region_colour,
			ax,ay,aw,ah,
			fill=True
		).draw_rect(
			region_border,
			ax,ay,aw,ah,
			fill=False
		)
		
		#debugging
		#annotations.write_to_file('/tmp/1.png')
		
		alw, alh = annotations.get('width'), annotations.get('height')
		
		print(" - Compositing...")
		output = output.composite(minimap,'over',x=int(w-(tw+ox)),y=int(h-(th+oy)))
		# need to convert to srgb to be able to composite
		annotations = annotations.copy(interpretation='srgb')
		
		output = output.composite(annotations,'over',x=int(w-(tw+ox))-2,y=int(h-(th+oy))-2)
		
		return output
		
	@staticmethod
	def get_minimap(filename:str, image:Image = None, minimap_size:int = 1024)-> tuple:
		"""
		returns (image, scalefactor)
		Generate or load the minimap at the desired size for the specified file
		we use a fairly large default size (1024) here because an image this size
		 can be scaled quickly and will give higher quality minimaps at larger
		 sizes (as opposed to scaling up a smaller minimap)
		"""
		
		if image is None:
			image = load_image(filename)
		
		minimapfile = os.path.join(os.path.dirname(filename), "minimap." + os.path.splitext(os.path.basename(filename))[0] + ".jpg")
		
		imgw, imgh = get_size(image)
		
		#thumbnail height/width
		th = tw = minimap_size
		
		if False: # this branch means 'minimap size is the smallest dimension'
			if imgw > imgh:
				tw = int(round(th * (imgw/imgh)))
			elif imgw != imgh:
				th = int(round(tw * (imgh/imgw)))
		else:
			if imgw > imgh:
				th = int(round(tw * (imgh/imgw)))
			elif imgw != imgh:
				tw = int(round(th * (imgw/imgh)))
			
		if os.path.exists(minimapfile):
			# use cached minimap
			#print(" - Using cached minimap")
			minimap = Image.new_from_file(minimapfile)
			#minimap height/width
			mmw, mmh = get_size(minimap)
			if (max(mmw,mmh)) != minimap_size:
				# resize minimap:
				minimap = minimap.resize(minimap_size/max(mmw,mmh))
		else:
			"""
			HACK:
			
			When generating minimaps, we always generate them at a minimap size of 1024 pixels.
			
			We do this because we have the --minimap-size option, i.e the user can set a 
			 size arbitrarily.
			
			Since we use a cached minimap and thumbnailing a huge image is expensive,
			 we simply scale the cached minimap to the desired size at runtime.
			 
			To make this work (relatively) smoothly and quickly, we always
			 generate the minimap at a (relatively, for a minimap) large size
			 so that if the user passes a large value to --minimap-size they
			 don't get a horribly pixellated mess.
			 
			It may be worth making the generate-minimap size fine-tunable at
			 some point.
			 
			The awful hack that we do here is that we simply generate the image
			 and then return the response from another call to get_minimap
			"""
			
			if imgw > imgh:
				th = int(round(1024 * (imgh/imgw)))
				tw = 1024
			elif imgw != imgh:
				tw = int(round(1024 * (imgw/imgh)))
				th = 1024
			else:
				tw = th = 1024
			
			print(f" - Generating {tw}x{th} minimap '{os.path.basename(minimapfile)}'...")
			minimap = image.thumbnail_image(tw,height=th)
			
			minimap.jpegsave(
				minimapfile,
				Q=90
			)
			
			return DeepFieldSource.get_minimap(filename,image,minimap_size)
		
		return (minimap, tw / imgw, tw, th)
