# sessions.py
# part of radrequests
# 

import sys, os, hashlib, pickle, time, tempfile, glob

from tqdm import tqdm
import requests as realrequests

CACHE_DIR='/tmp/.radrequests.cache'

if not os.path.exists(CACHE_DIR):
	os.mkdir(CACHE_DIR)

def sha(val:str) -> str:
	"""
	A simplified shasum helper, takes in a string and gives back a string
	"""
	return hashlib.sha1(str(val).encode('utf8')).hexdigest()

class Session(realrequests.Session):
	"""
	The RadRequests replacement for requests.Session
	
	This is where all our magick is done, since all requests methods end up
	 here.
	
	"""
	
	__debug=False
	
	def log(self,text):
		if self.__debug:
			print(text)
	
	def cleanup_cache(self):
		"""
		Clean up any cache files that have expired
		"""
		now = time.time()
		cleaned=0
		#print(glob.glob(os.path.join(CACHE_DIR,"*.*")))
		for f in glob.glob(os.path.join(CACHE_DIR,"*.*")):
			_, expiry = os.path.basename(f).split('.',1)
			if int(expiry) < now:
				os.unlink(f)
				cleaned+=1
				
		return cleaned
		#if cleaned>0:
		#	self.log(f"radrequests.cleanup_cache: Cleaned up {cleaned} items")
	
	def get_cached(self,url:str,age:int=30,**kwargs):
		"""
		return a cached version of the URL if it is available and less than age minutes old
		"""
		self.log(f"Radrequests.Session.get_cached({url},{age})")
		
		hash = sha(url)
		expiry = int(round(time.time() + (age*60)))
		
		cachefile=os.path.join(CACHE_DIR,hash)
		
		matches = glob.glob(cachefile + "*") #glob expiry
		if len(matches)>0:
			#there should only be 1 match
			existing=matches[0]
			
			dt = os.path.getmtime(existing)
			file_age = (time.time() - dt) / 60
			if file_age < age:
				file_age = "%2.1f" % file_age
				self.log(f" - reading cache {existing} ({file_age} mins old)")
				with open(existing,'rb') as f:
					ret = pickle.load(f)
					# we need to reimplement save_as for cache hits to have
					# it work... but should it?
					if kwargs['save_as'] is not None:
						with open(kwargs['save_as'],'wb') as d:
							d.write(ret._content)
						self.log(f"Wrote {kwargs['save_as']}")
					ret.cached = True
					ret.cachefile = existing
					# If we have a cache hit, we've probably saved some time,
					# so now is also a good time to clean up our cache
					self.cleanup_cache()
					return ret
			else:
				#TODO: if the cache file is older than age, send an 
				# if-modified-since header and use the cached response if
				# not modified.
				self.log(f" - cachefile {existing} too old ({file_age} mins)")
				os.unlink(existing) #we'll write a new one with a new expiry
		
		ret = self.request('get',url,cache_mins=None,**kwargs)
		cachefile+="." + str(expiry) # add exoiry to the file we're creating
		with open(cachefile,'wb') as f:
			#dave cache:
			self.log(f" - writing cache {cachefile}")
			pickle.dump(ret,f,protocol=pickle.HIGHEST_PROTOCOL)
		ret.cached = False
		ret.cachefile = None
		return ret
		
	
	def download(self,url,save_as:str,progressbar:bool=True,**kwargs):
		"""
		Helper to easily download a file
		"""
		return self.request('get',url,progressbar=progressbar,save_as=save_as,**kwargs)
		
	
	def get_with_fallback(self,method,url, progressbar:bool = False, cache_mins:int = None, save_as:str = None, **kwargs):
		"""
		Extension to .get. Only call this if you expect the URL to exist.
		If it doesn't, this helper will try to retrieve it from archive.org
		"""
		pass
	
	def request(self,method, url, progressbar:bool = False, cache_mins:int = None, save_as:str = None, **kwargs):
		"""
		Perform a HTTP request with the requests library.
		
		Supports all the parameters supported by requests.Session().request(), PLUS:
		
		:param	progressbar	optional Union[bool,str]	true/false to turn on/off, 'auto' to have progressbar if runnning in a tty
		
		:param	cache_mins	optional int				if provided and a cached version is available and less than cache_mins old,
														 the cached response is returned. If provided and no cache (or too old), a
														 new cache file is written
														 
		:param	save_as		optional str				provide a filename to have the response saved here.
		"""
		self.log(f"Radrequests.Session.request({method},{url},{kwargs})")
		
		if str(method).lower() == 'get' and cache_mins is not None:
			kwargs['progressbar'] = progressbar
			kwargs['save_as'] = save_as
			for i in ['cache_mins', 'age']:
				if i in kwargs:
					del kwargs[i]
			return self.get_cached(url,age=cache_mins,**kwargs)
		
		if progressbar == 'auto':
			progressbar = sys.stdout.isatty()
		progressbar = bool(progressbar)
		
		savefile = save_as
		if progressbar:
			kwargs['stream'] = True #must stream for progressbar
		
		response = realrequests.Session.request(self,method,url,**kwargs)
		
		total_size_in_bytes= int(response.headers.get('content-length', 0))
		
		block_size = 1024 #1 Kilobyte
		
		got_bytes = 0 
		
		if savefile is None and progressbar:
			savefile = tempfile.TemporaryFile()
		elif savefile is not None:
			savefile = open(savefile,'wb+')
			
		if progressbar:
			with tqdm(total=total_size_in_bytes, unit='iB', unit_scale=True) as progress_bar:
				for data in response.iter_content(block_size):
					got_bytes += len(data)
					progress_bar.update(len(data))
					savefile.write(data) # shit
				savefile.seek(0)
				response._content_consumed = False
				response._content = savefile.read() #shit
		
		if savefile is not None:
			if not progressbar:
				savefile.write(response._content)
			savefile.close()
		if save_as is not None:
			self.log(f"Wrote {save_as}")
		return response
