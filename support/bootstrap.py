# bootstrap.py - loads awpgen into sys.path, changes us to the project
# directory, loads venv, and loads all helpers

import sys
from os.path import exists, join, dirname, basename

def load_venv(directory) -> bool:
	"""
	Attempt to load a venv at the specified directory by adding it to sys.path
	 this negates the need for loading a venv before running your code :)
	"""
	venv_libdir = join(directory,'lib')
	if exists(venv_libdir) and venv_libdir not in sys.path:
		print(f"loading venv at {venv_libdir}...")
		sys.path.append(venv_libdir)
		packagedir = join(
			venv_libdir,
			"python" + (".".join(sys.version.split('.',2)[0:2])),
			'site-packages'
		)
		if exists(packagedir):
			#print(f" - adding '{packagedir}' to path...")
			sys.path.append(packagedir)
		return True
	else:
		return False

project_dir = dirname(dirname(__file__))
print(f"Bootstrapping in {project_dir}...")
# add the project to the path
sys.path.append(project_dir)

# add the venv:
venv = join(project_dir,'venv')
load_venv(venv)

from core.helpers import *
