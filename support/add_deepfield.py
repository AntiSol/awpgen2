#!/usr/bin/env python3
# add_deepfield.py - adds a deepfield image to the library

import os, sys, json

from bootstrap import *

from waybackpy import WaybackMachineSaveAPI

from plugins.deepfield import DeepFieldSource

print("""AWPGEN2 - Add Deepfield

This tool will download a deepfield image, analyse it, prompt you for info, 
 and then add it to the deepfield library so that it is available in the 
 list.
 
""")

library = DeepFieldSource.load_library()

image_url = filename = None

if len(sys.argv)>1:
	image_url = sys.argv[1]
	#if the "url" is a file, we're in test mode (i.e testing this script)
	if os.path.exists(image_url):
		print("Using local file")
		filename=image_url
		image_url = input("URL for this file: ")
	
else: 
	print("Usage: add_deepfield.py <image url>")
	sys.exit(0)

image_url = str(image_url).strip()

title=input(f"Enter A single-line title for the image at '{image_url}':\n\n").strip()
#image_url = input("Enter the URL for the image file: ")
print()

source_url=input(f"Enter the source URL where we should point a browser:\n").strip()

date = input("Release Year: ")
release_id = input("Release ID (optional): ")


if filename is None:
	ok, filename, request = download_file(image_url,'/tmp/')

filebytes = os.path.getsize(filename)
filesize = friendly_size(filebytes)

print(f"Done, got {filesize}.")
image = load_image(filename)
h,w = get_size(image)

pixels=friendly_pixelcount(w,h)
print(f"Image is {w}x{h} ({pixels})")

pre_optimised = False # set to true if your input image has already been optimised
if pre_optimised:
	optbytes=filebytes
else:
	DeepFieldSource.optimise_image(filename,filename + '.optimised.tiff')
	optbytes = os.path.getsize(filename + '.optimised.tiff')
	print(f"Done, optimised image is {friendly_size(optbytes)}")


#print("Available fields: ",image.get_fields())
#imgdate = find_date(image)
#print("date",imgdate)

description = get_description(image)

if not description and yesno("Do you want to enter a description (Y/N)?"):
	print("\nType a description. Newlines are ok. Enter a blank line to finish.\n")
	ret=""
	while (not ret) or (line != ""):
		line = input("")
		if line != "":
			ret += "\n" + line
	description=ret
else:
	print(f"Image Description: {description}\n")

# This is our item which we're saving into the library:
item = {
	'title': title.strip(),						# Image Title (one line)
	'url': image_url.strip(),					# URL for the image file
	'bytes': filebytes,							# Bytes to download
	'bytes_optimised': optbytes,						# Bytes used on disk when optimised
	'width': w,									# Width
	'height': h,								# Height
	'description': description.strip(),			# multi-line description
	'filename': os.path.basename(filename),		# filename on disk. saved as 'deepfield_' + filename
	'source': source_url.strip(),				# Source URL, e.g to point a web browser at.
	'id': release_id,									# Release ID, if there is one, e.g heic0611b 
	'date': date						# Release Date
}

print("OK, we have everything we need to add to the library. This is what we'll be adding:\n\n")
print(json.dumps(item,sort_keys=True, indent=4))
print()
if not yesno():
	sys.exit(1)

library.append(item)

library_file = os.path.join(project_dir,"plugins","deepfields.json")
save_json(library,library_file)

print(f"Saved {library_file}")

print(f"Asking archive.org to take a snapshot of '{image_url}'...")
wayback = WaybackMachineSaveAPI(image_url, user_agent=user_agent())
archived_url = wayback.save()
print(f"Archived at '{archived_url}'")

print(f"Asking archive.org to take a snapshot of '{source_url}'...")
wayback = WaybackMachineSaveAPI(source_url, user_agent=user_agent())
archived_url = wayback.save()
print(f"Archived at '{archived_url}'")

print("All Done, if you survive, plese come again!\n")

