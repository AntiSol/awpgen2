#!/usr/bin/env python3
# archive_library.py - asks archive.org to take a snapshot of each
# image / page in the library
# You probably shouldn't be running this, it kinda spams archive.org,
#  and the add_deepfield script asks archive.org to take a copy anyway

import os, sys, json, time

from bootstrap import *

from waybackpy import WaybackMachineSaveAPI

from plugins.deepfield import DeepFieldSource

archives=[]
n=0
for item in DeepFieldSource.load_library():
	n+=1
	api = WaybackMachineSaveAPI(item['url'], user_agent=user_agent())
	print(f"{n}: Archiving '{item['title']}' ({item['url']})...")
	url = api.save()
	print(f" - saved as: {url}\n")
	archives.append(url)
	
	api = WaybackMachineSaveAPI(item['source'], user_agent=user_agent())
	print(f"{n}: Archiving source ({item['source']})...")
	url = api.save()
	print(f" - saved as: {url}\n")
	archives.append(url)
	
	time.sleep(5) # be less spammy

print("All done!")

print("Archive URLs:\n" + ("\n ".join(archives)))

