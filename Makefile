# AntiSol's pymagic Makefile
# (C) Dale Magee, 2022
# BSD 3-clause license
# 
# This makefile provides a couple of handy features for python development,
#	and is intended to be easily extended to suit the needs of your project.
#
# HOWTO:
# * First, Put a requirements.txt file in the same directory as the Makefile
#	 (ideally your project root)
#
# * Next, edit the EXECUTABLE variable below.
#
# * then, type 'make help' to invoke the makefile's automatic help system.
#	 You can add new targets to the makefile with docstring comments below 
#	 them, and 'make help' will show these automatically.
#	 Docstring lines start with '#?'
#
# * 'make build' builds a new virtual environment in the directory
#	 specified by VENV below and installs requirements.
#
# * 'make clean-venv' nukes it
# 
# * type 'make test' to build/activate the venv as necessary and run pytest
#
# * type 'make run' to run the program in the venv
#
# * These are the basics, there are other commands available, see 'make help'
#

# the executable that 'make run' calls (relative to current dir)
EXECUTABLE=awpgen

# default target. You could also specify e.g 'build' or 'run' here, to taste.
all: help

# name of requirements.txt
REQS=requirements.txt

# directory for venv
VENV=venv

# name of gawk command
GAWK=gawk

help:
#? mysterious
	@echo "Makefile for python executable '${EXECUTABLE}'."
	@echo -e "\nUsage: make [target]"
	
	@ #error checks:
	@( which ${GAWK} >/dev/null || { echo "\nError: ${GAWK} is needed to display help properly. Please install it.\n" && exit 1; } )
	@( test -f "${REQS}" || { echo -e "\nError: You need a requirements file '${REQS}'. Please create one.\n" && exit 1; } )
	@( test -x "${EXECUTABLE}" || { echo -e "\nError: You need an executable file '${EXECUTABLE}' with execute permissions. Please create one.\n" && exit 1; } )
	@ #show help:
	@echo -e "\nThe default target is '`awk '/^all: /{print $$2}' Makefile`'"
	@gawk 'match($$0,/^([a-z\-]+):/,n) {CMD=n[1];CANHAS=""}; match($$0,/^#\?\s*(.*)$$/,n) { if (CMD) { if (CANHAS) print "  "n[1]; else print "\nmake "CMD"\n  "n[1] ; CANHAS="1" } }  ' Makefile
	@echo

run:
#? runs $EXECUTABLE, building and activating the venv as necessary
	@( test -d ${VENV} || make build )
	@( . ${VENV}/bin/activate && ./${EXECUTABLE} )

test:
#? runs the test suite
	@( test -d ${VENV} || make build )
	@( . ${VENV}/bin/activate && pytest )

build: clean-venv
#? Cleans and rebuilds the virtual environment.
	@echo Building virtual environment in ${VENV}...
	@( python3 -m venv ${VENV}; \
		. ${VENV}/bin/activate; \
		pip install --upgrade pip wheel; \
		pip install -r ${REQS}; \
	)
	@echo -e "\nDone, use './${EXECUTABLE}' or 'make run' to launch the program."

update:
#? Updates an existing venv when requirements have been modified
	@echo Updating venv...
	@( . ${VENV}/bin/activate && pip install -r ${REQS} )

upgrade:
#? Updates all packages in the venv (hopefully?) 
#DM: not quite sure if this works...
	@echo Upgrading all packages in venv...
	@( . ${VENV}/bin/activate; \
		pip install -r <(pip freeze | cut -d= -f1) --upgrade \
	)
	make test

freeze:
#? Freeze the current venv into the requirements file
	@( . ${VENV}/bin/activate && pip freeze > ${REQS} )
	@echo "\nFroze venv into ${REQS}\n"

activate-venv:
#? Activates the virtual environment.
#? Note: hacky and horrible, you shouldn't use this on the CLI,
#?  '. venv/bin/activate' is better.
#  (it's actually starting a sub-sub-shell and make is still running!)
	@echo Activating virtual environment in a subshell...
	@bash -c "bash --rcfile <(echo '. ~/.bashrc; . ${VENV}/bin/activate;')"

clean:
#? Cleanup the project.
	@echo Cleanup...
	@find . -name \*.pyc -delete

clean-all: clean clean-venv
#? Cleanup both venv and project.

clean-venv:
#? Cleanup the virtual environment.
	@echo Cleanup virtual environment...
	@rm -Rf ${VENV}
	
coffee:
	@echo -e "\nI'm not that good\n"
	@exit 42
