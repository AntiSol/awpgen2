AWPGEN - The Astronomical Wallpaper Generator v2

By Dale Maggee

Space is big. Really big. You just won't believe how vastly, hugely, mindbogglingly big it is... Unless you have the Astronomical Wallpapaer Generator.

AWPGen is a program for geeks to have their wallpaper change randomly, with enough images available that you're not going to get bored any time soon.

AWPGen is a command-line tool for generating interesting astronomy-themed images randomly, ostensibly for use in a cron job overwriting your desktop wallpaper at a regular interval.

***AWPGEN 2***

This version of awpgen is a ["from-scratch rewrite"](https://www.jwz.org/doc/cadt.html) in python, using pyvips, with several goals in mind:

* ability to handle arbitrarily large images efficiently, removing the huge memory requirements of the ruby version and imagemagick
	- do this by ensuring that deepfield images are stored in tiled tiff format, so that we don't need sequential access, and by using the vips library, which is much more efficient than imagemagick.
	- downloader auto-converts into tiled tiff
	- can auto-convert files which are not tiled tiff(optionally?)

* perhaps use nasa's APIs rather than scraping the APOD site? Maybe both?
	- probably not the APOD API, this requires an API key, so it's annoying to set up.
	- integration of other image sources?
	- https://antisol.org/gallery/astro.json

* multi-monitor support: chop a huge image into 3 to show on 3 monitors,
	or show 3 different images

(the following dox are from the old ruby v1 implementation)



awpgen has 2 modes of operation:

1. Hubble Mode

Hubble Mode uses the Hubble Ultra Deep Field image (or, in fact, any large image), which is absurdly large in size. So large, in fact, that no standard monitor can display the entire image properly: The scaled down image you see on your puny computer monitor misses all the fine detail in this image, and it's the amazing detail, and the huge number of tiny pinpoints of light, each of which is a galaxy, which makes this picture so cool. Hubble Mode takes a random section of this image which is the size of your screen at 1:1 resolution, allowing you to be almost-properly astounded by the sheer vastness of the observable universe.

You can place one or more deep field images in ~/.awpgen/ with a filename starting with 'deepfield', and by default awpgen will randomly select one of these images when hubble mode is invoked. 

You can use the '-l' and '-g' command-line parameters to do this automatically - the '-l' switch lists known deep field images, and '-g idx' allows you to download these images (where idx is a number from the list)

If no deep field images are found, awpgen will try to download one (The 2014 Hubble Ultra Deep Field, relatively small file).

You can also use the -d command-line parameter to specify a particular deep field image. Most image formats are supported (anything supported by ImageMagick / RMagick).


2. APOD Mode

I've discovered that even random sections of huge Deep-field images can become boring after a few years. So, in order to vary things up even more, we have APOD Mode.
APOD Mode uses NASA's "Astronomy Picture Of The Day" website to get a random astronomy image. Since we're looking for pretty wallpapers, we apply certain restrictions on what images we retrieve from the APOD Site:

 - we have a user-modifyable black list which allows us to exclude any image we want.
 	awpgen handles "exclusions files" - any file matching ~/.awpgen/exclusions.* will be processed. 
	Exclusions files contain pages to ignore, one per line. lines starting with "#" are comments.
	Anything awpgen excludes automatically goes into ~/.awpgen/exclusions.auto
	Anything excluded with --suckage goes into ~/.awpgen/exclusions.user
 
 - we have a "--suckage" command line parameter which nicely adds the most recently downloaded image to the blacklist, and generates another wallpaper.
 
 - we only retrieve jpeg, png, and tif images, since these seem to be NASA's preferred formats for big, pretty images on the APOD site. The APOD site does use a bunch of other formats ranging from gifs to youtube videos, but on the whole it looks like these aren't usually worth putting on your wallpaper - they're charts and low-res pics which wouldn't scale nicely.
 
 - we only accept images that are at least a certain size. This prevents us from seeing horrible upscaled blurs when the APOD image is small. By default, this size is 640x480, but this can be configured.
 
 - we're clever: When an image fails these tests, it's automagically blacklisted, so that we don't waste time or bandwidth trying to use it again.
 	(~/.awpgen/exclusions.auto)
 
The retrieved APOD image will be scaled to fill your screen resolution. By default, this means cropping in most cases (few images are the same Aspect Ratio as your screen). This behaviour can be changed, with the image scaled to fit. There's also an option to disable scaling altogether.

We cache the APOD index file, only retrieving it once per day.

APOD mode will only make a certain number of attempts to get an APOD image before falling back to hubble mode, so if your intarwebs has gone away your wallpaper will still change (to a hubble-mode image)

Images available:

The APOD Website has over 6000 beautiful images available - this grows daily, hence the "of the day" part in the name.

The Hubble Deep Field Image contains 384440000 pixels (38.4MP) - this is 20 times the area of your screen if you run at full HD resolution (1920x1080), and 50 times your screen area if you run at 1024x768.

awpgen now has a list of multiple deep field images, ranging from 'big' to 'insanely huge'. use '-l' and '-g' to list/retrieve them.

Command line options:

run 'awpgen -h'.

