#!/usr/bin/env python3
#######################################
# AWPGEN2
#######################################
# The Astronomical Wallpaper Generator
#  version 2
# By Dale Magee, 2022
# BSD 3-clause license

# system imports
import sys, os, random, glob, tempfile, argparse,math, atexit
from datetime import datetime

"""
Magick to auto-load a venv if there is one...
(need to find python version and load appropriate site packages)
"""
venv_libdir = os.path.join(os.path.dirname(__file__),'venv','lib')
#print(venv_libdir)
if os.path.exists(venv_libdir) and venv_libdir not in sys.path:
	#print(f"Auto-loading venv at {venv_libdir}...")
	sys.path.append(venv_libdir)
	packagedir = os.path.join(
		venv_libdir,
		"python" + (".".join(sys.version.split('.',2)[0:2])),
		'site-packages'
	)
	if os.path.exists(packagedir):
		#print(f" - adding '{packagedir}' to path...")
		sys.path.append(packagedir)


#This is an evil hack. Does it work? Yes!(?)
import requests
from core.helpers import user_agent
requests.utils.default_user_agent = user_agent


# package imports
from pyvips import Image
from screeninfo import get_monitors

# local imports
from core.helpers import *
from core.source import find_source_plugins
from plugins import *

def save_tiff(image: Image, filename:str):
	ret= image.tiffsave(
		filename,
		tile=True,
		bigtiff=True,
		pyramid=False,
		compression='jpeg',		# we use jpeg compression in our tiled tiff
		Q=80,					# for a relatively small file size
	)
	
	print(f"Wrote '{filename}'")
	return ret



def is_tiled(image:Image) -> bool:
	return image.get_typeof("vips-sequential") == 0

def convert_to_tiled_tiff(sourcefile: str, destfile: str):
	"""
	Convert an input image into a tiled tiff
	"""
	print(f"Converting '{sourcefile}' into tiled tiff '{destfile}'...")
	image = Image.new_from_file(sourcefile)
	return save_tiff(image,destfile)

def is_tiff(filename:str) -> bool:
	_,ext = os.path.splitext(filename.lower())
	return ext in [".tiff",".tif"]

def rand_region(filename:str, w:int=None, h:int=None,scale:float=1):
	"""
	Extract a random region of the specified size from the speficied image file.
	"""
	#if not is_tiff(filename):
	#	print(f"Warning: file '{filename}' is not a tiff, \n  This means it will be relatively slow and memory-intensive to process.\n  Consider converting it to a tiled tiff for better performance.")
	
	image = Image.new_from_file(filename,access="sequential")
	#if image.sequential:
	#	print("ERROR: Image is not sequentially accessible, please convert it to a tiled tiff!")
	#	sys.exit(666)
	
	if not is_tiled(image):
		print(f"WARNING: '{filename}' is not saved in a tiled format optimised for random access!\n You should let awpgen convert it to a tiled tiff for much much faster execution and smaller memory footprint.")
		image = Image.new_from_file(filename)
		#return None
	
	imgw, imgh = image.get('width'), image.get('height')
	try:
		desc = image.get('image-description')
	except Exception as ex:
		desc = None
	
	dw, dh = desktop_size()
	if w is None or h is None:
		if w is None: w = dw
		if h is None: h = dh
	
	#print(f" - Loaded {filename}")
	factor = "%1.2f" % ((imgw*imgh) / (dw*dh))
	print(f" - Image is {imgw}x{imgh} ({factor}x your desktop)")
	print(f" - Description: {desc}")
	
	#print(dir(image))
	#print(image.get_fields())
	
	w /= scale
	h /= scale
	w=int(w)
	h=int(h)
	
	#if the screen is too big for the image, scale it up:
	if w > imgw:
		nscale=w/imgw
		image = image.resize(nscale)
		imgh = int(round(imgh * (nscale)))
		imgw = w
		nscale = "%1.3f" % nscale
		print(f" - Desktop wider than image, scaled to {imgw}x{imgh} ({nscale}x)")
		
	if h > imgh:
		nscale=h/imgh
		image = image.resize(nscale)
		imgw = int(round(imgw * (nscale)))
		imgh = h
		scale = "%1.3f" % nscale
		print(f" - Desktop taller than image, scaled to {imgw}x{imgh} ({nscale}x)")
	
	x = random.randint(0,int(imgw - w))
	y = random.randint(0,int(imgh - h))
	
	print(f" - Cropping {w}x{h}+{x}x{y}")
	return image.crop(x,y,w,h).resize(scale)
	
	#print("Writing...")
	#crop.write_to_file('/tmp/awpgen_image.jpg')
	
	#print("Done!")
	

def desktop_size():
	"""
	Figure out the bounding box for all monitors
	"""
	x=None
	y=None
	w=0
	h=0
	for m in get_monitors():
		
		if x is None: x = m.x
		elif m.x < x: x = m.x
		
		if y is None: y = m.y
		elif m.y < y: y = m.y
		
		if m.x + m.width > w: w = m.x + m.width
		if m.y + m.height > h: h = m.y + m.height
		
	return (w-x,h-y)
	
def find_deepfields():
	"""
	Return a list of all deepfield images found in the search paths
	"""
	paths = [
		'~/.awpgen',
		'/usr/share/awpgen',
		'/usr/local/share/awpgen',
	]
	
	ret = []
	for path in paths:
		path = os.path.expanduser(path)
		files = glob.glob(os.path.join(path,'deepfield*'))
		ret += files
	#print(ret)
	return ret
	

	
def deepfield_mode():
	"""
	Get a random desktop-sized region from a random deepfield image.
	"""
	print("* Deepfield Mode")
	deepfields = find_deepfields()
	filename = random.choice(deepfields)
	filesize = friendly_size(os.path.getsize(filename))
	
	print(f" - Using deepfield '{filename}' ({filesize})")
	#print(deepfields)
	scale = 1.0
	w,h = desktop_size()
	return rand_region(filename,w,h,scale)
	
def split_for_monitors(image,monitors):
	"""
	extracts a region from the image for each monitor
	"""
	ret = []
	if len(monitors) < 2: #nothing to do!
		return [(monitors,image)]
	print(" - Splitting for monitors..")
	# find duplicate monitors and ignore them
	deduped=[]
	for m in monitors:
		dupe=False
		dupe_of=None
		for m2 in deduped:
			if (m2.x == m.x and
				m2.y == m.y and
				m2.width == m.width and
				m2.height == m.height
			):
				dupe=True
				dupe_of=m2.name
				break
				
		if not dupe:
			deduped.append(m)
		else:
			print(f"   - Monitor '{m.name}' is a duplicate of '{dupe_of}', ignoring")
	
	w, h = get_size(image)
	for monitor in deduped:
		print(f"   - {monitor.name}: {monitor.x}x{monitor.y}+{monitor.width}x{monitor.height}")
		
		cw = monitor.width
		ch = monitor.height
		
		if monitor.x + monitor.width > w:
			cw = w - monitor.x
		
		if monitor.y + monitor.height > h:
			ch = h - monitor.y
		
		monimg = image.crop(monitor.x, monitor.y, cw, ch)
		ret.append( ( monitor, monimg) )
		
	return ret

	
startTime = datetime.now()
def show_elapsed():	
	elapsed = datetime.now() - startTime
	print(f"Elapsed: {elapsed}")
	
if __name__ == "__main__":
	
	print("AWPGEN2 - The Astronomical Wallpaper Generator v2.0")
	atexit.register(show_elapsed)
	plugins = find_source_plugins()
	
	source_descriptions = {name: cls.description() for name,cls in plugins.items()}
	
	source_list = ("-"*40) + "\nAvailable Image Sources:\n"
	for name,desc in source_descriptions.items():
		source_list += f" {name.ljust(15)} {desc}\n"
	
	random.seed()
	#rand_region('/home/antisol/heic1502a_40k.tif')
	
	parser = argparse.ArgumentParser(
		description="Generates astronomical wallpapers from a variety of sources",
		epilog=source_list,
		formatter_class=argparse.RawDescriptionHelpFormatter
	)
	
	parser.add_argument("-o","--output",type=str,
		dest="output",metavar="filename",help="Filename to save to"
	)
	
	
	parser.add_argument("-m","--mode",type=str,
		dest="mode",metavar="mode",help="Specify an Image Source Plugin",choices=plugins.keys()
	)
	
	for name in plugins:
		parser.add_argument(f'--{name}',dest="mode",action="store_const",const=name,help=f"{name} mode, same as '-m {name}'")
	
	parser.add_argument('-1','--no-multimonitor', help="Disable multimonitor support",
		dest="multimonitor", action='store_false'
	)
	
	##########
	# General options - options used by multiple source plugins
	genopts = parser.add_argument_group("General options","These options are used by many source plugins")
	
	
	genopts.add_argument(
		'--title-offset',type=str,dest="title_offset",metavar="X,Y",
		help="Set Title / Minimap offset"
	)

	# set up arguments for source plugins:
	for name,plugin in plugins.items():
		group = parser.add_argument_group(f"{name} options")
		#print(dir(group))
		#print(group._actions)
		plugin.setup_args(group)
	
	parser.set_defaults(
		mode='random',
		multimonitor= True,
		run_and_exit=None,	# set this to run a method and exit
		title_offset="0,0",
	)
	
	opts = parser.parse_args()
	
	if opts.run_and_exit is not None:
		retval=opts.run_and_exit(opts)
		if retval is None: retval = 0	# if you don't return anything, it's considered "OK"
		retval=int(retval)
		sys.exit(retval)
	
	plugin = plugins[opts.mode]()
	print(f"Using {plugin.name()} image source")
	
	image = None
	attempts=0
	max_attempts=1
	while attempts < max_attempts and not isinstance(image,Image):
		attempts+=1
		if attempts>1:
			print(f"(try #{attempts})")
		image = plugin.get_image(opts)	
		if not isinstance(image,Image):
			print(f"Plugin Error: '{plugin.name()}' plugin returned invalid response: '{image}'")
	
	if not isinstance(image,Image):
		print(f"Fatal: Could not get an image from the {plugin.name()} plugin after {attempts} attempts!")
		#TODO: fallback to another plugin (but not random)
		sys.exit(1)
	
	if False:
		convert_to_tiled_tiff(
			'/home/antisol/.awpgen/deepfield_STSCI-H-p1917b-f-20791x19201.tiff',
			'/home/antisol/.awpgen/deepfield_STSCI-H-p1917b-f-20791x19201.optimised.tiff'
		)
		
		img = None
		attempt=0
		while img is None:
			img = deepfield_mode()
			if img is None:
				attempt+=1
				print(f"Deepfield mode failed, retrying (#{attempt})...")
		
	images = split_for_monitors(image,get_monitors())
	
	#progress_cb()
	#time.sleep(1)
	
	print("Working...")
	if len(images)==1:
		outfile = os.path.expanduser('~/awpgen_image.jpg')
		save_jpg(image,outfile)
	else:
		for monitor, image in images:
			#print(monitor, image)
			outfile = os.path.expanduser(f'~/awpgen_image_{monitor.name}.jpg')
			#image.write_to_file(outfile)
			save_jpg(image,outfile)
			
	plugin.done()
	

