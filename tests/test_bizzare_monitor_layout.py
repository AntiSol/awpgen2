

def test_bizarre_monitor_layouts():
	"""
	Test a variety of odd monitor layouts.	
	
	awpgen finds a rect with the bounding box containing all monitors,
	 then extracts a region corresponding to each monitor's location

	thus, it supports any monitor layout, even exotic ones

	Layouts to test:

	12

	123

	1234

	 1
    234

    1
    2

     1
    234
     5

	123
	456
	789

	"""
	assert False
