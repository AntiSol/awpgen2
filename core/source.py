"""
source.py

part of awpgen2

"""

#system imports
from typing import List

#package imports
from pyvips import Image

class AwpgenSource():
	"""
	The base class for image source plugins. 
	"""
	def __init__(self,config=None):
		"""
		Plugins should not override this method, override init() instead
		"""
		self._config = config
		self.init(config)
	
	def init(self,config=None):
		"""
		an init helper for plugins
		"""
		pass
	
	@classmethod
	def name(cls) -> str:
		"""
		Plugins must override this method.
		Return a string containing the name of your plugin, i.e "deepfield" or "apod".
		This string will be used to look up your plugin.
		"""
		return None
		
	@classmethod
	def description(cls) -> str:
		"""
		Plugins should override this method
		Provide a description of your plugin here
		"""
		return cls.name() + " plugin"

	@classmethod
	def setup_args(cls,parser):
		"""
		Plugins may override this to add extra command-line arguments to awpgen
		
		The parser is an argparser to which you can add arguments.
		
		as a special case, you may use the following pattern to have an argument
		 run a method on your plugin:
		 
		 
		def some_method(opts):
		   return 0 # return value is an integer exit code.
		
		def setup_args(cls,parser):
			parser.add_argument('--foo',action='store_const',dest="run_and_exit",const=some_method)
		
		"""
		return parser
	
	def get_image(self, opts) -> Image:
		"""
		Plugins must override this method
		This is the heavy lifter: it is called by awpgen and returns a wallpaper image
		opts is an argparse namespace providing options
		"""
		pass
		
	def done(self):
		"""
		Called on cleanup.
		"""
		pass
	

def find_source_plugins():
	"""
	Return a dict containing available plugins { <name:str>: plugin_class: Class, ... }
	"""
	return { cls.name(): cls for cls in AwpgenSource.__subclasses__() }
