# helpers.py
# generic helpers for awpgen

VERSION=2.0

import math, tempfile, sys, os, json

from array import array

from screeninfo import get_monitors
from pyvips import Image

from radrequests import requests

from tqdm import tqdm

class NoRandomAccesError(Exception):
	pass

def yesno(prompt="Proceed (Y/N)?") -> bool:
    """ 
    Prompt the user for a yes/no-ish response and return it as a boolean
    """
    resp=input(prompt + " ")
    if resp.strip().upper()[0] == "Y":
        return True
    return False
    
def save_json(data,filename:str):
	with open(filename,"w") as f:
		f.write(json.dumps(data, sort_keys=True, indent=4))
		

def sort_by(data,fieldname,cast_to:None):
	"""
	Given a list of dicts, sorts it by item['fieldname']
	cast_to should be a basic type, e.g int or str, if None, no casting is done
	"""
	if callable(cast_to):
		return sorted(data, key=lambda i: cast_to(i[fieldname]))
	
	return sorted(data, key=lambda i: i[fieldname])

def find_date(image:Image) -> str:
	"""
	Looks through available fields for anything like a date and returns
	 the first non-empty value that we find
	"""
	for field in image.get_fields():
		if 'date' in field.lower():
			print(f" - Examining field {field}") 
		
			val = str(image.get(field)).strip()
			if val:
				return val

def friendly_size(bytes:int,decimals:int=2) -> str:
	"""
	Convert a number of bytes into a hunam-readable size
	"""
	scales=['bytes','kB','MB','GB','TB','PB','EB','ZB','YB']
	idx = int(math.log(bytes,1024))
	return f"%1.{decimals}f %s" % (bytes/(1024**idx),scales[idx])

def friendly_pixelcount(width:int,height:int,decimals:int=1) -> str:
	"""
	Similar to friendly_size, but for images - Returns a friendly string 
	 like "4.2 gigapixels" given an image width and height
	"""
	scales=['pixels','kilopixels','megapixels','gigapixels','terapixels',
		'petapixels','exopixels','zettapixels','yottapixels'
	]
	pixels = width * height
	idx = int(math.log(pixels,1000))
	return f"%1.{decimals}f %s" % (pixels/(1000**idx),scales[idx])
	

def is_tiled(image) -> bool:
	return image.get_typeof("vips-sequential") == 0
	
def desktop_size():
	"""
	Figure out the bounding box for all monitors
	"""
	x=None
	y=None
	w=0
	h=0
	for m in get_monitors():
		
		if x is None: x = m.x
		elif m.x < x: x = m.x
		
		if y is None: y = m.y
		elif m.y < y: y = m.y
		
		if m.x + m.width > w: w = m.x + m.width
		if m.y + m.height > h: h = m.y + m.height
		
	return (w-x,h-y)
	
def transparent_image(width:int=260,height:int=260)->Image:
	"""
	Generate a new transparent image of arbitrary size
	"""
	return Image.black(width, height).copy(interpretation="srgb").new_from_image([0, 0, 0, 0])
	
def load_image(filename: str, ensure_tiled:bool=False) -> Image:
	"""
	Load an image with pyvips, optionally raising an error if the file isn't
	 in a format allowing random access
	:param ensure_tiled 	bool	if true and the image doesn't allow access, raise a NoRandomAccessEror
	"""
	image = Image.new_from_file(filename,access="sequential")
	if not is_tiled(image):
		if ensure_tiled:
			raise NoRandomAccesError(f"Error: '{filename}' is not in a format which allows random access, cannot open!")
		#if we're not ensuring tiled, load the image without the access param:
		image = Image.new_from_file(filename)
	return image
	
def save_jpg(image: Image,filename:str,quality=90):
	# we write to a temporary file first, since pyvips truncates the file
	#  before doing its work:
	with tempfile.NamedTemporaryFile(suffix=".jpg") as f:
		ret = image.jpegsave(
			f.name,
			Q=quality
		)
		# once written, we copy that data to our dest file
		with open(filename,'wb') as dest:
			dest.write(f.read())
		print(f"Wrote '{filename}'")
	return ret
	
def get_size(image:Image) -> tuple:
	"""
	return (w,h) size of image
	"""
	return (image.get('width'), image.get('height'))

def get_description(image:Image) -> str:
	"""
	Returns a string with the image description, if there is one, or None
	"""
	try:
		return image.get('image-description')
	except Exception as ex:
		return None

def user_agent():
	"""
	Return a user agent string for the program
	"""
	return "AWPGen v%1.3f, https://gitlab.com/antisol/awpgen2" % VERSION

def download_file(
	url:str,
	dest:str,
	use_content_disposition:bool=False,
	i_insist_it_exists:bool = False
):
	"""
	download a file, showing a progress bar

	dest may be a directory, in which case the filename will be derived from the URL

	if dest is a dir, and use_content_disposition is true, and the server sends
	 one, the filename will come from the content-disposition header
	
	TODO: If you insist the file exists, and there's some error, this method 
	 will try to download it from the internet archive.
	
	returns a tuple with success, the destination filename and the response object
	:return (ok, filename, request)
	"""
	#https://esahubble.org/media/archives/images/original/heic0611b.tif

	# Streaming, so we can iterate over the response.
	print(f"Downloading '{url}'...")
	response = requests.get(url,progressbar=False, stream=True)
	total_size_in_bytes= int(response.headers.get('content-length', 0))
	destfn = os.path.basename(url)
	
	disposition=response.headers.get('content-disposition')
	if use_content_disposition and disposition is not None:	
		#TODO: make this swork properly, parse the value and get the filename
		print('disposition',disposition)
	
	block_size = 1024 #1 Kilobyte
	
	if os.path.isdir(dest):
		# we must have a filename
		dest = os.path.join(dest,destfn)
		#print(f"saving as '{dest}')")
	got_bytes=0
	with tqdm(total=total_size_in_bytes, unit='iB', unit_scale=True) as progress_bar, open(dest, 'wb') as file:
		for data in response.iter_content(block_size):
			got_bytes += len(data)
			progress_bar.update(len(data))
			file.write(data)
	ok = (response.status_code == 200)
	if total_size_in_bytes != 0 and progress_bar.n != total_size_in_bytes:
		print(f"Warning: something went wrong! Expected {total_size_in_bytes} bytes, got {got_bytes}.")
		ok=False
	
	return (ok, dest, response)
	
def draw_text(text:str,
		height:int, width: int, 
		textcolour:list = [255,255,255], 
		fontname:str = "Arial", 
		fontfile:str = None
) -> Image:
	"""
	Draw the specified text, outlined in black, on a transparent background
	
	"""
	
	#see https://github.com/libvips/pyvips/issues/204#issuecomment-679245961
	if fontfile is not None:
		alpha = Image.text(text,height=height,width=width, font=fontname, fontfile=fontfile)
	else:
		alpha = Image.text(text,height=height,width=width, font=fontname) #.copy(interpretation="srgb")
		
	black = alpha.new_from_image([0, 0, 0]).bandjoin(alpha).copy(interpretation="srgb")
	colour = alpha.new_from_image(textcolour).bandjoin(alpha).copy(interpretation="srgb")
	
	tw, th = get_size(alpha)
	
	output = transparent_image(tw+8,th+8)
	
	for by in [0,4]:
		for bx in [0,4]:
			output = output.composite(black,'over',x=bx+2,y=by+2)
	
	#output = output.gaussblur(2)
	
	output = output.composite(colour,'over',x=4,y=4)
	
	return output
	

def fitscale(image:Image,width:int,height:int) -> Image:
	"""
	Scales the image (up or down) so that the smallest side will be the same
	 size as the corresponding dimension - a technical way of saying 
	 "zoom scale"
	"""
	iw, ih = get_size(image)
	
	# scale so that the smallest edge is the size of that dimension on the desktop
	# this will cause horrible cropping for portrait images
	
	sc=1
	ow, oh = (iw,ih)
	
	if iw > width and ih > height:
		if iw > width:
			psc = width/iw
			ph = ih*psc
			if ph>=height:
				sc = psc
				iw = width
				ih = ph
		if ih > height:
			psc = height/ih
			pw = iw*psc
			if pw >= width:
				sc = psc
				iw = pw
				ih = height
	
	if iw < width:
		sc = width / iw
		iw = width
		ih *= sc
		
	
	if ih < height:
		sc *= height / ih
		ih = height
		iw *= sc
		
	if sc != 1:
		image = image.resize(sc)
		w,h = get_size(image)
		sct = "%1.2f" % sc
		print(f" - Scaled image by {sct}: {ow}x{oh} -> {w}x{h}")
		
	return image
